import Text.Read
import System.Environment
import System.IO

type Task = (Int, Bool, String)     -- id, done?, description
type Todo = (Int, [Task])           -- nextId, tasks


afficheraide :: IO ()
afficheraide = do
    putStrLn ("print")
    putStrLn ("print todo")
    putStrLn ("print done")
    putStrLn ("add <string>")
    putStrLn ("do <int>")
    putStrLn ("undo <int>")
    putStrLn ("del <int>")
    putStrLn ("exit")

afficher :: Task -> IO ()
afficher (id,  done, txt)
    | done = putStrLn("[X] " ++ show id ++ ". " ++ show txt)
    | otherwise = putStrLn("[-] " ++ show id ++ ". " ++ show txt)

afficherAFaire:: [Task] -> IO ()
afficherAFaire [] = putStrLn("")
afficherAFaire  ((id, done, txt) : ts) 
        | not(done) = afficher (id, done, txt)
        | otherwise = afficherAFaire ts

afficherFait:: [Task] -> IO ()
afficherFait [] = putStrLn("")
afficherFait  ((id, done, txt) : ts) 
        | done = afficher (id, done, txt)
        | otherwise = afficherFait ts

setIndexOfTodo :: Task -> Bool -> Task
setIndexOfTodo (id, done, txt) b = (id, b, txt)

taskdone :: Int -> [Task] -> Task
taskdone _ [] = (0, False, "") :: Task
taskdone n ((id, done, txt) : ts)  = do
        if (id == n) then setIndexOfTodo (id, done, txt) True
        else taskdone n ts

taskundone :: Int -> [Task] -> Task
taskundone _ [] = (0, False, "") :: Task
taskundone n ((id, done, txt) : ts)  = do
        if (id == n) then setIndexOfTodo (id, done, txt) False
        else taskundone n ts

recupTask :: [Task] -> IO ()
recupTask [] = putStrLn("")
recupTask (t : ts)= do
    afficher t
    recupTask ts

nothing :: IO ()
nothing = putStr("")



menu :: Todo -> IO ()
menu todo = do
    putStr("> ")
    line <- getLine
    let val = words line -- Au moins je récupère les données des do !
    let base = (val !! 0) 
    let valeur = (val !! 1)
    let valint = read valeur :: Int
    
    case line of
        "print" -> recupTask (snd(todo) :: [Task])
        "print todo" -> afficherAFaire (snd(todo) :: [Task])
        "print done" -> afficherFait (snd(todo) :: [Task])    
        _ -> afficheraide
    
        
    if (line /= "exit") then menu todo
    else putStrLn("Au revoir!")


main :: IO ()
main = do
    contents <- readFile "tasks.txt"
    let todo1 = read $! contents :: Todo
    menu todo1
    print todo1
    mapM_ print (snd todo1)
