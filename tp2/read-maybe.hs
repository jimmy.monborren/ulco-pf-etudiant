import System.Environment
import Text.Read

main :: IO()
main = do
    line <- getLine
    case readMaybe line :: Maybe Int of
        Just x -> putStrLn (show x)
        Nothing -> putStrLn "nothing"
    

    