formateNull :: Int -> String
formateNull n = show n ++ " est " ++ b
    where b = case n of
            0 -> "nul"
            _ -> "non nul"

formateNull2 :: Int -> String
formateNull2 0 = "0 est nul"
formateNull2 n = show n ++ " est non nul"

main = do
    putStrLn (formateNull 5)
    putStrLn (formateNull 0)

    putStrLn (formateNull2 5)
    putStrLn (formateNull2 0)