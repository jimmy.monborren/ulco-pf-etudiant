safesqrt :: Double -> Double
safesqrt x = sqrt x

main = do
    putStrLn ("result : " ++ show (safesqrt 25))
    putStrLn ("result : " ++ show (safesqrt -25))