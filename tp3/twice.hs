twice :: [a] -> [a]
twice values = values ++ values

main = do
    line <- getLine

    print(twice line)