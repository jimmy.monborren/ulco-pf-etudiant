isPalindrome :: String -> Bool
isPalindrome w = w == reverse w

main :: IO()
main = do
    line <- getLine
    print (isPalindrome (line))