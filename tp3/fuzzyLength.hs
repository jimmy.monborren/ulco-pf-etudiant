fuzzyLength :: [a] -> string
fuzzyLength [] = "empty"
fuzzyLength [a] = "one"
fuzzyLength [a, b] = "two"
fuzzyLength _ = "many"