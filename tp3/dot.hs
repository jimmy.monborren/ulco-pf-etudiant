dot :: [Double] -> [Double] -> Double
dot x y = sum $ zipWith (*) x y

norm :: [Double] -> [Double]
norm x = sqrt $ dot x x