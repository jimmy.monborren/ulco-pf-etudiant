import System.Random

prendremot :: String -- mot à trouver choisi aléatoirement
prendremot = do
    let motpossible = ["function", "functional programming", "haskell", "list", "pattern matching", "recursion", "tuple", "type system"]
    let random = randomRIO(0, 8 :: Int)
    (motpossible !! random)
    

remplacer :: Char -> Int -> Int -> String -> String -- est censé remplacer la valeur n d'une chaine de caractère... (ici l'ensemble des ?)
remplacer c x n [] = "Pas de valeur"
remplacer c x n (a : cs) = if (x == n) then c : remplacer c (x+1) n cs
                           else a : remplacer c (x+1) n cs


loopmot :: Char -> Int -> String -> String -> String -- Vérification du mot
--loopmot _ 0 []  = ""
loopmot cha n motactuel (x : xs) = if(x == cha) then remplacer (cha 0 n motactuel) : loopmot cha (n-1) motactuel xs -- Je ne sais pas pourquoi ça ne veut pas fonctionner
                    else loopmot cha (n-1) motactuel xs
    
contains :: String -> Char -> Bool -- Vérifier qu'une chaine de caractère contient un caractère
contains (x : xs) c = 
    if (x == c) then True
    else contains xs c

state :: Int -> String -- reprend le status en fonction du nombre de chance qu'il reste
state 0 = "     __________      \n\ 
\     |/        |     \n\
\     |        (_)    \n\
\     |       \\|/    \n\
\     |         |     \n\
\     |        / \    \n\
\ ____|______________ " -- state 0 = perdu
state 1 = "     __________      \n\
\     |/        |     \n\
\     |        (_)    \n\
\     |       \\|/    \n\
\     |         |     \n\
\     |            \n\
\ ____|______________ " -- state 1 = reste 1 chance
state 2 = "     __________      \n\
          \     |/           \n\
          \     |            \n\
          \     |          \n\
          \     |           \n\
          \     |            \n\
          \ ____|______________ " -- state 2 = reste 2 chance
state 3 = "           \n\
\     |/           \n\
\     |            \n\
\     |          \n\  
\     |           \n\
\     |            \n\
\ ____|______________ " -- state 3 = reste 3 chance
state 4 = "           \n\
\                \n\
\                 \n\
\               \n\
\                \n\
\                 \n\
\ ___________________ " -- state 4 = reste 4 chance

makemot :: String -> String -- Rempli le mot de base par des ?
makemot (x : xs)= '?' : makemot xs


loop :: Int -> IO () -- fait une boucle pour demander une réaction au joueur
loop 0 = putStrLn("Vous avez perdu!") -- c'est perdu
loop n = do  
    let mot = prendremot -- mot à trouver
    let motactuel = makemot mot -- récupère le mot rempli de ?


    print(state n) -- affichage du status des chance
    line <- getLine -- récupère une ligne
    print(loopmot (line !! 0) length(mot) motactuel mot) -- lance la boucle pour vérifier si il y a des choses à modifier ou non
    
    if(contains mot (line !! 0)) then loop n -- si ça contient la lettre alors laisser n chance, sinon faire perdre une chance
    else loop (n-1)


main = do 
    loop 4
