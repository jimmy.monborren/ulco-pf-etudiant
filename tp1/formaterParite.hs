formaterParite :: Double -> String
formaterParite n =
    if (n > 1)
        then putStrLn show n ++ " -> 0.0"
        else putStrLn show n ++ " -> " ++ show n)

main :: IO ()
main = do
    formaterParite -1.2
    formaterParite 0.2