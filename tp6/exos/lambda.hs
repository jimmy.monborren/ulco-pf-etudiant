mapmul  :: [Int] -> [Int] 
mapmul = map (\x -> x * 2)

mapdiv  :: [Int] -> [Int] 
mapdiv = map (\x -> x `div` 2)

main = do
    print(mapmul [1..4])
    print(mapdiv [1..4])
    print(map (\ (c,i) -> [c] ++ "-" ++ show i) (zip ['a' .. 'z'] [1 .. ]))
    print(zipWith (\ c i -> [c] ++ "-" ++ show i) ['a' .. 'z'] [1 .. ])
    print(zipWith (\ c -> \ i -> [c] ++ "-" ++ show i) ['a' .. 'z'] [1 .. ])