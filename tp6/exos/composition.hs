plus42Positif :: Int -> Bool
plus42Positif = (> 0) . (+ 42) 

mul2PlusUn :: Integer -> Integer
mul2PlusUn = (+ 1) . (* 2)

mul2moinUn :: Integer -> Integer
mul2moinUn =  (+(-1)) . (* 2)

applyTwice :: (Integer -> Integer) -> Integer -> Integer
applyTwice f = f . f

main = do
    print (plus42Positif 2)
    print (plus42Positif (-84))
    print (mul2PlusUn 10)
    print (mul2moinUn 10)
    print (applyTwice mul2PlusUn 10)