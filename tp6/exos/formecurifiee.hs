myRangeTuple1 :: (Int,Int) ->[Int] 
myRangeTuple1 (a,b) = [a..b] 

myRangeCurry :: Int -> Int -> [Int]
myRangeCurry a b = [a..b]

myRangeTuple2 :: Int ->[Int] 
myRangeTuple2  a = myRangeTuple1 (0, a)

myRangeCurry2 :: Int -> [Int]
myRangeCurry2 a  = myRangeCurry 0 a

myCurry :: ((Int, Int) -> [Int]) -> Int -> Int -> [Int]
myCurry f1 x0 x1 = f1 (x0, x1)

main = do
    print(myRangeTuple1 (0, 9))
    print(myRangeCurry 0 9)
    print(myRangeTuple2 9)
    print(myRangeCurry2 9)

