
myTake2  :: [Int] -> [Int] 
myTake2 = take 2

myGet2  :: [Int] -> Int 
myGet2 = (!! 2)

mapmul2  :: [Int] -> [Int] 
mapmul2 = map (*2)

mapdiv2  :: [Int] -> [Int] 
mapdiv2 = map (`div` 2)

main = do
    print(myTake2 [1..4])
    print(myGet2 [1..4])
    print(mapmul2 [1..4])
    print(mapdiv2 [1..4])
