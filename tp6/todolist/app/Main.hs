{-# LANGUAGE OverloadedStrings #-}

import qualified GI.Gtk as Gtk
import Data.Text

handleButton :: IO ()
handleButton = putStrLn "hello"

main :: IO ()
main = do
    _ <- Gtk.init Nothing
    window <- Gtk.windowNew Gtk.WindowTypeToplevel 
    Gtk.windowSetDefaultSize window 600 100
    Gtk.windowSetTitle window "Hello World!"
    _ <- Gtk.onWidgetDestroy window Gtk.mainQuit

    --label <- Gtk.labelNew (Just "Hello World!")
    --Gtk.containerAdd window label

    button <- Gtk.buttonNewWithLabel ("Ajouter")
    Gtk.containerAdd window button
    _ <- Gtk.onButtonClicked button handleButton

    button1 <- Gtk.buttonNewWithLabel ("Ajouter")
    Gtk.containerAdd window button1
    _ <- Gtk.onButtonClicked button1 handleButton

    Gtk.widgetShowAll window
    Gtk.main
