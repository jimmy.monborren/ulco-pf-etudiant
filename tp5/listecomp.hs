import Data.Char

doubler :: [Int] -> [Int]
doubler xs = [ 2 * x | x <- xs ]

pairs :: [Int] -> [Int]
pairs xs = [ x | x <- xs, even x ]

mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f xs = [ f x | x <- xs]

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x : xs) = [ x | x <- xs, f x ] 

multiples :: Int -> [Int]
multiples n = [x | x <- [1 .. n], n `mod` x == 0]

combinaison :: [a] -> [b] -> [(a, b)]
combinaison tba tbb = [ (x, y) | x <- tba, y <- tbb ]

tripletsPyth :: Int -> [(Int, Int, Int)]
tripletsPyth n = [(x, y, z) | x <- [1 .. n], y <- [x .. n], z <- [1 .. n], (x * x) + (y * y) == (z * z)]


decaler :: Int -> Char -> Char
decaler n c = chr(ord (c) + n)

dechiffrerCesar :: Int -> String -> String
dechiffrerCesar n (x:xs)= decaler (-n) x : dechiffrerCesar n xs

chiffrerCesar :: Int -> String -> String
chiffrerCesar n (x:xs)= decaler n x : chiffrerCesar n xs

main :: IO()
main = do
    print(doubler [1..4])
    print(pairs [1..4])
    print(mymap (*2) [1..4])
    print(myfilter even [1..4])
    print(multiples 35)
    print(combinaison ["pomme", "poire"] [13, 37, 42])
    print(tripletsPyth 13)
    print(chiffrerCesar 1 "hal")
    print(dechiffrerCesar 1 "ibm")