mapDoubler1 :: [Int] -> [Int]
mapDoubler1 [] = []
mapDoubler1 (x : xs) = x * 2 : mapDoubler1 xs

mapDoubler2 :: [Int]->[Int]
mapDoubler2 [] = []
mapDoubler2 xs = map (* 2) xs

mymap :: (a -> b) -> [a] -> [b]
mymap _ [] = []
mymap f (x:xs) = f x : mymap f xs


filterEven1 :: [Int] -> [Int]
filterEven1 [] = []
filterEven1 (x : xs) = if even x then x : filterEven1 xs
                       else filterEven1 xs

filterEven2 :: [Int] -> [Int]
filterEven2 = filter even

myfilter :: (a -> Bool) -> [a] -> [a]
myfilter _ [] = []
myfilter f (x : xs) = if f x then x : myfilter f xs
                      else myfilter f xs


foldSum1 :: [Int] -> Int
foldSum1 [] = 0
foldSum1 (x : xs) = x + foldSum1 xs

foldSum2 :: [Int] -> Int
foldSum2 xs = foldl (+) 0 xs   

myfoldl :: (b -> a -> b) -> b -> [a] -> b
myfoldl _ acc [] = acc
myfoldl f acc (x : xs) = myfoldl f (f acc x) xs

main :: IO()
main = do
    print(mapDoubler1 [1..4])
    print(mymap (*2) [1..4])
    print(myfilter (even) [1..4])
    print(foldSum1 [1..4])

