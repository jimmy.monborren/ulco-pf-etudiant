mylast :: [a] -> a
mylast (x : []) = x
mylast (_ : xs) = mylast xs

myinit :: [a] -> [a]
myinit (_ : []) = []
myinit (x : xs) = x : myinit xs

myreplicate :: Int -> Char -> [Char]
myreplicate 0 _ = []
myreplicate n c = c : myreplicate (n -1) c

mytake :: Int -> [Char] -> [Char]
mytake 0 _ = []
mytake n (x : xs) = x : mytake (n -1) xs

mydrop :: Int -> [Char] -> [Char]
mydrop 0 (x : xs) = x : xs
mydrop n (_ : xs) = mydrop (n -1) xs

main = do
    print(mylast "foobar")
    print(myinit "foobar")
    print(myreplicate 4 'f')
    print(mydrop 3 "foobar")
    print(mytake 3 "foobar")