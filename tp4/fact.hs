factoriel :: Integer -> Integer
factoriel 0 = 1
factoriel x = x * factoriel(x-1)

aux 0 x = x
aux n x = aux(n-1) (x * n)

factTco :: Integer -> Integer
factTco n = aux (n-1) 1

main = do
    print(factoriel 2)
    print(factoriel 5)
    print(factTco 2)
    print(factTco 5)