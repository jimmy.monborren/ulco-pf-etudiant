loopecho :: Int -> IO String
loopecho 0 = return "loop terminated"
loopecho x = do
    putStr "> "
    valeur <- getLine
    if valeur == "" then return "empty line"
    else do
        putStrLn valeur
        loopecho (x-1)

main :: IO ()
main = do
    result <- loopecho 3

    putStrLn result