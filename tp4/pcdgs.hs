pcgd :: Integer -> Integer -> Integer
pcgd a 0 = a
pcgd a b = pcgd b (mod a b)

main = do
    print(pcgd 49 35)