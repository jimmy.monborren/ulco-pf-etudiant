mulLIste :: Int -> [Int] -> [Int]
mulLIste _ [] = []
mulLIste n (x : xs) = (n * x) : (mulLIste n xs)

selectListe :: (Int, Int) -> [Int] -> [Int]
selectListe _ [] = []
selectListe (xMin, xMax) (x:xs) = 
    if(x <= xMax) && (x >= xMin)  
        then x : selectListe (xMin, xMax) xs
    else
        selectListe (xMin, xMax) xs

sumListe :: [Int] -> Int
sumListe [] = 0
sumListe (x : xs) = x + sumListe xs

sumListeD :: [Double] -> Double
sumListeD [] = 0
sumListeD (x : xs) = x + sumListeD xs

main = do
    print (mulLIste 3 [3, 4, 1])
    print (selectListe (2, 3) [3, 4, 1])
    print (sumListe [3, 4, 1])
    print (sumListeD [3, 4, 1.0::Double])