import Data.Char

mylength :: [a] -> Int
mylength [] = 0
mylength (_ : xs) = 1 + mylength xs

toUpperS :: String -> String
toUpperS "" = ""
toUpperS (x : xs) = toUpper x : toUpperS xs

onlyLetters :: String -> String
onlyLetters "" = ""
onlyLetters (x : xs) = 
    if isLetter x
        then x : onlyLetters xs
        else onlyLetters xs

main = do
    print (mylength "foobar")
    print (toUpperS "foobar")
    print (onlyLetters "foo9b6a4r")
